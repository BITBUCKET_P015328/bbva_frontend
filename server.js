var express = require('express'),
app = express(),
port = process.env.PORT || 2000;

var path = require('path');
app.use(express.static(__dirname + '/build/v1'));

app.listen(port);

console.log('todo list Polymer API server started on: '+port);

app.get("/",function(req,res){
  res.sendFile('index.html',{root:'.'});
})
